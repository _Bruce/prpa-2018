#include "sobel.hh"

Filters::Sobel::Sobel(cv::Mat *a)
    : Filter(parallel),
    m(a)
{}

Filters::Sobel::Sobel()
    : Filter(parallel),
    m(nullptr)
{}

Filters::Sobel::~Sobel()
{
    delete m;
}

void Filters::Sobel::setMat(cv::Mat *a)
{
    this->m = a;
}

void Filters::Sobel::filterApply(cv::Mat& a)
{
    for (int i = 0; i < a.rows - 3; ++i)
    {
        for (int j = 0; j < a.cols - 3; ++j)
        {
            float mag_X = 0.0;
            float mag_Y = 0.0;
            for (int a_0 = 0; a_0 < 3; ++a_0)
            {
                for (int b = 0; b < 3; ++b)
                {
                    mag_Y += a.at<cv::Vec3b>(i + a_0, j + b)[0] * mat_y[a_0][b];
                    mag_Y += a.at<cv::Vec3b>(i + a_0, j + b)[1] * mat_y[a_0][b];
                    mag_Y += a.at<cv::Vec3b>(i + a_0, j + b)[2] * mat_y[a_0][b];
                    mag_X += a.at<cv::Vec3b>(i + a_0, j + b)[0] * mat_x[a_0][b];
                    mag_X += a.at<cv::Vec3b>(i + a_0, j + b)[1] * mat_x[a_0][b];
                    mag_X += a.at<cv::Vec3b>(i + a_0, j + b)[2] * mat_x[a_0][b];
                }
            }
            a.at<cv::Vec3b>(i, j)[0] = std::sqrt((mag_X * mag_X) + (mag_Y * mag_Y));
            a.at<cv::Vec3b>(i, j)[1] = std::sqrt((mag_X * mag_X) + (mag_Y * mag_Y));
            a.at<cv::Vec3b>(i, j)[2] = std::sqrt((mag_X * mag_X) + (mag_Y * mag_Y));
        }
    }
}

void *Filters::Sobel::operator()(void *a)
{
    if (a)
    {
        auto v = (std::vector<cv::Mat>*) a;
        for (auto tmp : *v)
        {
            filterApply(tmp);
        }
        return a;
    }
    else
        return nullptr;
}

void Filters::Sobel::operator()(const tbb::blocked_range<size_t>& r) const
{
    return;
}
