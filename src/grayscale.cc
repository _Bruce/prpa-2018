#include "grayscale.hh"

Filters::Grayscale::Grayscale(cv::Mat* a)
    : Filter(parallel),
    m(a)
{}

Filters::Grayscale::Grayscale()
    : Filter(parallel)
{}

Filters::Grayscale::~Grayscale()
{}

void Filters::Grayscale::setMat(cv::Mat *a)
{
    this->m = a;
}

void Filters::Grayscale::filterApply(cv::Mat &a)
{
    for (int i = 0; i < a.rows; ++i)
    {
        for (int j = 0; j < a.cols; ++j)
        {
            auto pix = a.at<cv::Vec3b>(i, j);
            int rgb_ = (pix[0] + pix[1] + pix[2])/3;
            a.at<cv::Vec3b>(i, j)[0] = rgb_;
            a.at<cv::Vec3b>(i, j)[1] = rgb_;
            a.at<cv::Vec3b>(i, j)[2] = rgb_;
        }
    }
}

void *Filters::Grayscale::operator()(void *a)
{
    if (a)
    {
        auto v = (std::vector<cv::Mat>*) a;
        for (auto tmp : *v)
        {
            filterApply(tmp);
        }
        return a;
    }
    else
        return nullptr;
}

void Filters::Grayscale::operator()(const tbb::blocked_range<size_t>& r) const
{
    return;
}
