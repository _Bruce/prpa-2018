#pragma once
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include "filter.hh"

namespace Filters
{
    class Grayscale : public Filter
    {
    public:
        Grayscale();
        Grayscale(cv::Mat* a);
        ~Grayscale();
        void *operator()(void *a) override;
        void operator()(const tbb::blocked_range<size_t>& r) const override;
        void filterApply(cv::Mat& a) override;
        void setMat(cv::Mat *a) override;
    private:
        cv::Mat* m;
    };
}
