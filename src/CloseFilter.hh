#pragma once
#include <tbb/tbb.h>
#include <opencv2/opencv.hpp>

namespace Filters
{
    class CloseFilter : public tbb::filter
    {
    public:
        CloseFilter(std::string out, cv::VideoCapture& a);
        ~CloseFilter();
        void *operator()(void* item) override;
        cv::VideoWriter vw_;
        cv::VideoCapture vc_;
    };
}
