#pragma once
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include <tbb/parallel_while.h>
#include "grayscale.hh"
#include "binarize.hh"
#include "sobel.hh"
#include "overlay.hh"

namespace video
{
    class Video
    {
    public:
        Video();
        Video(std::string path);
        ~Video();
        void SeqApply(Filters::Filter& a, std::string out);
        void ParallelApply(Filters::Filter& a, std::string out);
    private:
        std::string path_;
        cv::VideoCapture vc_;
    };
}
