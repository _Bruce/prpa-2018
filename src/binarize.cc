#include "binarize.hh"

Filters::Binarize::Binarize(cv::Mat *a)
    : Filter(parallel),
    m(a)
{}

Filters::Binarize::Binarize()
    : Filter(parallel)
{}

Filters::Binarize::~Binarize()
{}

void Filters::Binarize::setMat(cv::Mat *a)
{
    this->m = a;
}

void Filters::Binarize::filterApply(cv::Mat& a)
{
    for (int i = 0; i < a.rows; ++i)
    {
        for (int j = 0; j < a.cols; ++j)
        {
            for (int z = 0; z < 3; ++z)
                a.at<cv::Vec3b>(i, j)[z] = (a.at<cv::Vec3b>(i, j)[z] <= 128 ? 0 : 255);
        }
    }
}

void *Filters::Binarize::operator()(void *a) 
{
    if (a)
    {
        auto v = (std::vector<cv::Mat>*) a;
        for (auto tmp : *v)
        {
            filterApply(tmp);
        }
        return a;
    }
    else
        return nullptr;
}
