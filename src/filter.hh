#pragma once
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>

namespace Filters
{
    class Filter : public tbb::filter
    {
    public:
        Filter(tbb::filter::mode m);
        Filter(cv::Mat *a, tbb::filter::mode m);
        ~Filter();
        virtual void operator()(const::tbb::blocked_range<size_t>& r) const;
        virtual void filterApply(cv::Mat& a);
        virtual void setMat(cv::Mat *a);
        virtual void *operator()(void *a) override;
    private:
    };
}
