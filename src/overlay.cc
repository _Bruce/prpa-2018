#include "overlay.hh"

Filters::Overlay::Overlay(cv::Mat* a, std::string path)
    : Filter(serial_out_of_order),
    m(a),
    path_(path),
    vc_(new cv::VideoCapture(path))
{}

Filters::Overlay::Overlay(std::string path)
    : Filter(serial_out_of_order),
    m(nullptr),
    path_(path),
    vc_(new cv::VideoCapture(path))
{}

Filters::Overlay::~Overlay()
{
    delete vc_;
}

void Filters::Overlay::setMat(cv::Mat *a)
{
    this->m = a;
}

void Filters::Overlay::filterApply(cv::Mat &a)
{
    cv::Mat tmp;
    if (vc_->read(tmp))
    {
        for (int i = 0; i < a.rows && i < tmp.rows; ++i)
        {
            for (int j = 0; j < a.cols && j < tmp.cols; ++j)
            {
                a.at<cv::Vec3b>(i, j)[0] =
                    a.at<cv::Vec3b>(i, j)[0] * 0.5 + tmp.at<cv::Vec3b>(i, j)[0]*0.5;
                a.at<cv::Vec3b>(i, j)[1] =
                    a.at<cv::Vec3b>(i, j)[1] * 0.5 + tmp.at<cv::Vec3b>(i, j)[1]*0.5;
                a.at<cv::Vec3b>(i, j)[2] =
                    a.at<cv::Vec3b>(i, j)[2] * 0.5 + tmp.at<cv::Vec3b>(i, j)[2]*0.5;
            }
        }
    }
}

void *Filters::Overlay::operator()(void *a)
{
    if (a)
    {
        auto v = (std::vector<cv::Mat>*) a;
        for (auto tmp : *v)
        {
            filterApply(tmp);
        }
        return a;
    }
    else
        return nullptr;
}

void Filters::Overlay::operator()(const tbb::blocked_range<size_t>& r) const
{
    return;
}
