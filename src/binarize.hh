#pragma once
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include "filter.hh"


namespace Filters
{
    class Binarize : public Filter
    {
    public:
        Binarize(cv::Mat *a);
        Binarize();
        ~Binarize();
        void *operator()(void *a) override;
        void filterApply(cv::Mat& a) override;
        void setMat(cv::Mat *a) override;
    private:
        cv::Mat *m;
    };
}
