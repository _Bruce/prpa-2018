#include "OpenFilter.hh"

Filters::OpenFilter::OpenFilter(std::string path)
    : filter(serial_in_order),
    vc_(path),
    a(nullptr)
{}

Filters::OpenFilter::~OpenFilter()
{
    delete a;
}

void* Filters::OpenFilter::operator()(void *item)
{
    cv::Mat frame;
    std::vector<cv::Mat> *a = new std::vector<cv::Mat>();
    std::cout << vc_.get(CV_CAP_PROP_POS_FRAMES) << std::endl;
    for (int i = 0; i < 75 && vc_.read(frame); ++i)
    {
        cv::Mat f;
        frame.copyTo(f);
        a->push_back(f);
    }
    if (a->size() == 0
            || (vc_.get(CV_CAP_PROP_POS_FRAMES) == vc_.get(CV_CAP_PROP_FRAME_COUNT)))
    {
        return nullptr;
    }
    return a;
}
