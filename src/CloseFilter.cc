#include "CloseFilter.hh"

namespace Filters
{
    CloseFilter::CloseFilter(std::string out, cv::VideoCapture& a)
        :filter(serial_in_order)
         , vc_(a)
    {
        vw_ = cv::VideoWriter(out, CV_FOURCC('M', 'J', 'P', 'G'),
                 a.get(CV_CAP_PROP_FPS),
                 cv::Size(a.get(CV_CAP_PROP_FRAME_WIDTH),
                         a.get(CV_CAP_PROP_FRAME_HEIGHT)),
                     true);
    }

    CloseFilter::~CloseFilter()
    {}

    void *CloseFilter::operator()(void *item)
    {
        std::vector<cv::Mat> *a = (std::vector<cv::Mat>*) item;
        for (auto z : *a)
            vw_.write(z);
        delete a;
        return nullptr;
    }
}
