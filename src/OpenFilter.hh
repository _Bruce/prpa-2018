#pragma once
#include "filter.hh"

namespace Filters
{
    class OpenFilter : public tbb::filter
    {
    public:
        OpenFilter(std::string path);
        virtual ~OpenFilter();
        void *operator()(void *item) override;
        cv::VideoCapture vc_;
        cv::Mat *a;
    };
}
