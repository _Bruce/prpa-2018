#include "video.hh"

video::Video::Video(std::string path)
    : vc_(cv::VideoCapture(path))
{}

video::Video::~Video()
{}

void video::Video::SeqApply(Filters::Filter& a, std::string out)
{
    if (!vc_.isOpened())
    {
        std::cout << "Opening file: " << this->path_ << " failed";
        std::exit(1);
    }
    else
    {
        cv::Mat frame;
        cv::VideoWriter c(out,
                        CV_FOURCC('F', 'M', 'P', '4'),
                        vc_.get(CV_CAP_PROP_FPS),
                        cv::Size(vc_.get(CV_CAP_PROP_FRAME_WIDTH),
                            vc_.get(CV_CAP_PROP_FRAME_HEIGHT)),
                        true);
        while (vc_.read(frame))
        {
            a.filterApply(frame);
            std::cout << "Processing frames: "
                << (vc_.get(CV_CAP_PROP_POS_FRAMES) / vc_.get(CV_CAP_PROP_FRAME_COUNT)) * 100
                << "%" << std::endl;
        }
        c.release();
    }
}

void video::Video::ParallelApply(Filters::Filter& a, std::string out)
{
    if (!vc_.isOpened())
    {
        std::cout << "Opening file: " << this->path_ << " failed";
        std::exit(1);
    }
    else
    {
        cv::Mat frame;
        cv::Mat t[50];
        cv::VideoWriter c(out,
                        CV_FOURCC('P', 'I', 'M', '1'),
                        vc_.get(CV_CAP_PROP_FPS),
                        cv::Size(vc_.get(CV_CAP_PROP_FRAME_WIDTH),
                            vc_.get(CV_CAP_PROP_FRAME_HEIGHT)),
                        true);
        tbb::pipeline pipe_;
        while (vc_.read(frame))
        {
            for (int i = 0; i < 50 && vc_.get(CV_CAP_PROP_POS_FRAMES)
                    < vc_.get(CV_CAP_PROP_FRAME_COUNT); ++i)
            {
                frame.copyTo(t[i]);
                vc_.read(frame);
            }
            a.setMat(t);
            tbb::parallel_for(tbb::blocked_range<size_t>(0, 50),
                    [&](const::tbb::blocked_range<size_t>& r) {
                    a(r);
                    });
            for (int i = 0; i < 50; ++i)
                c.write(t[i]);
            std::cout << "Processing frames: "
                << (vc_.get(CV_CAP_PROP_POS_FRAMES) / vc_.get(CV_CAP_PROP_FRAME_COUNT)) * 100
                << "%" << std::endl;
        }
        c.release();
    }
}
