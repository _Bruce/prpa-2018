#include <iostream>
#include <boost/program_options.hpp>
#include "video.hh"
#include "OpenFilter.hh"
#include "CloseFilter.hh"
#include "timer.hh"

void seq_execute(std::vector<void*> *vect, std::string input_path, std::string output_path)
{
    double time;
    cv::Mat frame;
    scoped_timer::scoped_timer a(time);
    cv::VideoCapture vc__(input_path);
    cv::VideoWriter vw__(output_path, CV_FOURCC('M', 'J', 'P', 'G'),
            vc__.get(CV_CAP_PROP_FPS),
            cv::Size(vc__.get(CV_CAP_PROP_FRAME_WIDTH),
                vc__.get(CV_CAP_PROP_FRAME_HEIGHT)),
            true);
    while (vc__.read(frame))
    {
        std::cout << vc__.get(CV_CAP_PROP_POS_FRAMES)
            << "/" << vc__.get(CV_CAP_PROP_FRAME_COUNT)
            << std::endl;
        for (auto z : *vect)
        {
            Filters::Filter *tmp = (Filters::Filter*) z;
            tmp->filterApply(frame);
        }
        vw__.write(frame);
    }
    vc__.release();
    vw__.release();
    std::cout << "Time elapsed (seq): " << time << std::endl;
}

void parallel_execute(std::vector<void*> *vect, std::string input_path, std::string output_path)
{
    Filters::OpenFilter open(input_path);
    Filters::CloseFilter close(output_path, open.vc_);
    double time;
    scoped_timer::scoped_timer a(time);
    tbb::pipeline pipe_;
    pipe_.add_filter(open);
    for (auto z : *vect)
    {
        Filters::Filter *tmp = (Filters::Filter*) z;
        pipe_.add_filter(*tmp);
    }
    pipe_.add_filter(close);
    pipe_.run(8);
    std::cout << "Time elapsed (parallel): " << time << std::endl;
}

std::vector<void*> *filtersSelect()
{
    auto ret = new std::vector<void*>();
    std::cout << "Select a filter from the current list:" << std::endl;
    std::cout << "1) Grayscale" << std::endl << "2) Binarize" << std::endl
        << "3) Sobel (edge detection)" << std::endl
        << "4) Overlay" << std::endl;
    Filters::Grayscale *g = new Filters::Grayscale();
    Filters::Binarize *b = new Filters::Binarize();
    Filters::Sobel *s = new Filters::Sobel();
    int a;
    bool jeej = true;
    while (std::cin >> a && jeej)
    {
        switch(a)
        {
            case 1:
                ret->push_back(g);
                break;
            case 2:
                ret->push_back(b);
                break;
            case 3:
                ret->push_back(s);
                break;
            case 4:
                std::string path;
                std::cin >> path;
                auto t = new Filters::Overlay(path);
                ret->push_back(t);
                break;
        }
    }
    return ret;
}

int main(int argc, char **argv)
{
    // Command line initialization
    boost::program_options::options_description o("options");
    std::string input_path;
    std::string output_path = "output.avi";
    o.add_options()
        ("help", "Print help")
        ("video_path,p",
         boost::program_options::value<std::string>(&input_path), "Path to the input video")
        ("output_path,o", boost::program_options::value<std::string>(&output_path),
         "Path to the output (by default, output.avi in the execution directory)");
    boost::program_options::variables_map vars;
    try
    {
        boost::program_options::store(
                boost::program_options::parse_command_line(argc, argv, o), vars);
        if (vars.size() == 0 || !vars.count("video_path"))
        {
            std::cerr << "Missing args" << std::endl;
            std::cerr << o << std::endl;
            return 1;
        }
        else
        {
            input_path = vars["video_path"].as<std::string>();
            if (vars.count("ouput_path"))
                output_path = vars["output_path"].as<std::string>();
            Filters::Sobel g;
            auto filtersList = filtersSelect();
            parallel_execute(filtersList, input_path, output_path);
        }
    }
    catch (boost::program_options::error& e)
    {
        std::cerr << "wtf: " << e.what() << std::endl;
        std::cerr << o << std::endl;
        return 1;
    }
    return 0;
}

