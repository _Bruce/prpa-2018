#pragma once
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include "filter.hh"


namespace Filters
{
    class Sobel : public Filter
    {
    public:
        Sobel(cv::Mat *a);
        Sobel();
        ~Sobel();
        void *operator()(void *a) override;
        void operator()(const tbb::blocked_range<size_t>& r) const override;
        void filterApply(cv::Mat& a) override;
        void setMat(cv::Mat *a) override;
    private:
        cv::Mat *m;
        float mat_x[3][3] = {{-1, 0, 1},
                             {-2, 0, 2},
                             {-1, 0, 1}};
        float mat_y[3][3] = {{-1, 2, -1},
                             {0, 0, 0},
                             {1, 2, 1}};
    };
}
