CXX=clang++ -pthread
CXXFLAGS=-std=c++1y -Wextra -Wall -g
LDLIBS=-lrt -ltbb -lboost_program_options
LDFLAGS+=`pkg-config --libs opencv`
TGT=prpa
SRC=src/main.cc\
    src/video.cc\
    src/grayscale.cc\
    src/binarize.cc\
    src/sobel.cc\
    src/filter.cc\
    src/overlay.cc\
    src/OpenFilter.cc\
    src/CloseFilter.cc
OBJ=${SRC:.cc=.o}

all: $(OBJ)
	$(CXX) $(CXXFLAGS) $(LDLIBS) $(LDFLAGS) $(OBJ) -o $(TGT)

clean:
	rm -f *.o
	rm -f src/*.o
	rm -f ${TGT}

.PHONY: clean
